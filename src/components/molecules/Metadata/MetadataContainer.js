import connector from '../../../store/connector';
import { Metadata } from './Metadata';
import {
  getCowcertsTransactions,
  getMetadataJson
} from '../../../selectors/certificate';

const mapStateToProps = (state) => ({
  metadataList: getMetadataJson(state),
  cowcertsTxs: getCowcertsTransactions(state)
});

const MetadataContainer = connector(Metadata, { mapStateToProps });
export { MetadataContainer };
