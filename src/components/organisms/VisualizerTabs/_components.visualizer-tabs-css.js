import {html} from '@polymer/lit-element';
export default html`<style>.buv-c-tabs{display:flex;float:left;margin-left:20px;align-items:flex-end}.buv-c-tab{min-width:5em;text-align:center;padding:1.25em 1em 0.5em 1em;cursor:pointer;height:3em;box-shadow:0 -2px 0 1px #ddd;background-color:#eee}.buv-c-tab.active{background-color:white}.buv-c-tab:hover{background-color:#f9f9f9}
</style>`;
